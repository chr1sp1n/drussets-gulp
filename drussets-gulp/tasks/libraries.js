'use strict';

const basePath = process.cwd();
const { src, dest } = require('gulp');
const rename = require("gulp-rename");
const colors = require('colors');
const notifier = require('./notifier');
const path = require('path');

module.exports = function(done){

	const config = require( path.join( basePath, '/drussets.config.json' ) );



	var libraries = config.js.libraries;
	var count = Object.keys(libraries).length;

	if(count == 0) {
		done();
		return;
	}

	Object.keys(libraries).forEach(function(library, index) {

	var filesFolder = [];
  var files = [];
  var fileIndex = 0;

	libraries[library].forEach( function( item ){
    if(typeof item == 'object'){
      item.files.forEach(function(file){
        filesFolder.push("./" + item.path);
        files.push( path.join( basePath, file ) );
      })
    }else{
      filesFolder.push("./js/");
      files.push( path.join( basePath, item ) );
    }
	});

	var destFolder = path.join( basePath, config.path.temp, config.assets.libraries, library );

	var task = src( files, { sourcemaps: config.sourcemaps } )
    .pipe(
      rename(
        function (file){
          file.dirname = filesFolder[fileIndex++];
        }
      )
    )
		.pipe(
      dest( destFolder, { sourcemaps: config.sourcemaps } )
        .on('error', function(){
          notifier.error('JavaScript library ' + colors.green(library) + ' error.');
          fail = 'LIBRARIES';
          done();
        })
      )
      .on('error', function(){
        notifier.error('JavaScript library ' + colors.green(library) + ' error.');
        fail = 'LIBRARIES';
        done();
      })
      .on('end', function(){
        notifier.log('JavaScript library ' + colors.green(library) + ' imported.');
        if(index == Object.keys(libraries).length - 1){
          done();
        }
      }
    );
	});

}

